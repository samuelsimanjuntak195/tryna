<?php

    // KONEKSI KE DATABASE
    $conn = mysqli_connect("localhost", "root", "", "phpdasar");
    
    // menerima PARAMETER DARI FUNCTION QUERY 
    function query($query) {
        // MENGAMBIL DATA DARI TABEL MAHASISWA LALU SIMPAN DALAM VARIABLE
        global $conn;
        $result = mysqli_query($conn, $query); //RESULT = ARRAY
        $rows = []; //TEMPAT UNTUK MENAMPUNG ELEMEN ARRAY
        // MENAMPILKAN SELURUH DATA DARI TABEL (ARRAY)
        while( $row = mysqli_fetch_assoc($result) ) {
            $rows[] = $row; //AMBIL DATA KEMUDIAN SIMPAN KE DALAM VAR
        }
        return $rows; //MENGEMBALIKAN DATA
    }

    // func MENERIMA INPUTAN DATA misal prameter = $data 
    // menampung data post yang dikirim oleh function tambah()
    function tambah($data) {
        global $conn;
        // GUNAKAN HTML SPECIAL CHARACTER
        $nama = htmlspecialchars($data['nama']);
        $nim = htmlspecialchars($data['nim']);  
        $email = htmlspecialchars($data['email']);
        $jurusan = htmlspecialchars($data['jurusan']);
        $gambar = htmlspecialchars($data['gambar']);

        // QUERY INSERT DATA
        $query = "INSERT INTO mahasiswa
                VALUES
                ('', '$nama', '$nim', '$email', '$jurusan', '$gambar')";
        
        // MENGIRIMKAN PERINTAH KE server mySql
        mysqli_query($conn, $query);

        return mysqli_affected_rows($conn);
    }

    // FUNCTIONS UNTUK MENGHAPUS DATA hapus.php

    // FUNCTION YANG MENERIMA PARAMETER $id 
    function hapus($id) {
        global $conn; //MEMANGGIL KONEKSI

        // PERINTAH SQL
        mysqli_query($conn, "DELETE FROM mahasiswa WHERE id = $id");
        // PERIKSA APAKAH DATA BERHASIL DIKIRIM
        return mysqli_affected_rows($conn);
    }

    // ubah.php  MENAMPUNG DATA POST YANG DIKIRIM 
    function ubah($data) {
        global $conn;

        // GUNAKAN HTML SPECIAL CHARACTER
        $id = $data['id']; //BUKAN USER YANG MENGINPUT
        $nama = htmlspecialchars($data['nama']);
        $nim = htmlspecialchars($data['nim']);  
        $email = htmlspecialchars($data['email']);
        $jurusan = htmlspecialchars($data['jurusan']);
        $gambar = htmlspecialchars($data['gambar']);

        // BUAT SEBUAH VARIABLE YANG MENAMPUNG PERINTAH SQL
        $query = "UPDATE mahasiswa 
                    SET
                    nama = '$nama',
                    nim = '$nim',
                    email = '$email',
                    jurusan = '$jurusan',
                    gambar = '$gambar'
                    WHERE id = $id   
                ";

        // JALANKAN QUERY
        //           koneksi, sintaks perintah sql ke server   
        mysqli_query($conn, $query);

        // RETURN ANGKA JIKA ADA DATA YANG BERHASIL DI UPDATE
        return mysqli_affected_rows($conn);
    }

    // CEARCHING FILE DATABASE index.php UNTUK FUNCTIONS CARI()
    function cari($keyword) {
        $query = "SELECT * FROM mahasiswa
            WHERE 
            nama LIKE '%$keyword%' OR
            nim LIKE '%$keyword%' OR
            email LIKE '%$keyword%' OR
            jurusan LIKE '%$keyword%' 
        ";
        return query($query);
    }
?>